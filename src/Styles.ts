import {css, SerializedStyles} from "@emotion/react";

export const MOBILE_VIEW = 540;
export const TABLET_VIEW = 768;
export const DESKTOP_VIEW = 1100;

export const breakpoints = {
    desktop: `${DESKTOP_VIEW}px`,
    mobile: `${MOBILE_VIEW}px`,
    tablet: `${TABLET_VIEW}px`,
};

/*-----------------------------------------------------TEMPLATES---------------------------------------------------*/
const itemStyles = css`
  position: relative;
`;
export const two_two = css`
  ${itemStyles}
  grid-column: span 1;
  grid-row: span 2;

  @media (min-width: ${breakpoints.desktop}) {
    grid-column: span 2;
    grid-row: span 2;
  }
`;
export const one_one = css`
  ${itemStyles}
  grid-column: span 1;
  grid-row: span 1;
`;
export const two_one = css`
  ${itemStyles}
  grid-column: span 1;
  grid-row: span 1;

  @media (min-width: ${breakpoints.desktop}) {
    grid-column: span 2;
    grid-row: span 1;
  }
`;

export enum ElementType {
    image,
    placeholder
}
export enum ElementOrentationType {
    horizontal,
    vertical
}
export type ElementFactorType = {
    css: SerializedStyles;
    type: ElementType;
    orientation: ElementOrentationType;
}

export type SliderPageTemplate = Array<ElementFactorType>

export const templateFC0:Array<ElementFactorType> = [
    {css: two_two, type: ElementType.image, orientation: ElementOrentationType.vertical},
    {css: one_one, type: ElementType.image, orientation: ElementOrentationType.vertical},
    {css: one_one, type: ElementType.image, orientation: ElementOrentationType.vertical},
    {css: two_two, type: ElementType.image, orientation: ElementOrentationType.vertical},
    {css: one_one, type: ElementType.placeholder, orientation: ElementOrentationType.vertical},
    {css: one_one, type: ElementType.image, orientation: ElementOrentationType.vertical},
];

export const templateFC1:Array<ElementFactorType> = [
    {css: two_two, type: ElementType.image, orientation: ElementOrentationType.vertical},
    {css: two_one, type: ElementType.image, orientation: ElementOrentationType.horizontal},
    {css: two_two, type: ElementType.image, orientation: ElementOrentationType.vertical},
    {css: one_one, type: ElementType.image, orientation: ElementOrentationType.vertical},
    {css: one_one, type: ElementType.placeholder, orientation: ElementOrentationType.horizontal},
];

export const templateFC2:Array<ElementFactorType> = [
    {css: two_two, type: ElementType.image, orientation: ElementOrentationType.vertical},
    {css: one_one, type: ElementType.image, orientation: ElementOrentationType.vertical},
    {css: one_one, type: ElementType.image, orientation: ElementOrentationType.vertical},
    {css: two_one, type: ElementType.image, orientation: ElementOrentationType.horizontal},
    {css: one_one, type: ElementType.image, orientation: ElementOrentationType.vertical},
    {css: one_one, type: ElementType.placeholder, orientation: ElementOrentationType.vertical},
    {css: two_one, type: ElementType.image, orientation: ElementOrentationType.horizontal},
];

export const templateFC3:Array<ElementFactorType> = [
    {css: two_one, type: ElementType.image, orientation: ElementOrentationType.horizontal},
    {css: two_one, type: ElementType.image, orientation: ElementOrentationType.horizontal},
    {css: two_two, type: ElementType.image, orientation: ElementOrentationType.vertical},
    {css: two_one, type: ElementType.image, orientation: ElementOrentationType.horizontal},
    {css: one_one, type: ElementType.image, orientation: ElementOrentationType.vertical},
    {css: one_one, type: ElementType.placeholder, orientation: ElementOrentationType.vertical},
];

export const templateFC4:Array<ElementFactorType> = [
    {css: two_one, type: ElementType.image, orientation: ElementOrentationType.horizontal},
    {css: two_one, type: ElementType.image, orientation: ElementOrentationType.horizontal},
    {css: one_one, type: ElementType.image, orientation: ElementOrentationType.vertical},
    {css: one_one, type: ElementType.image, orientation: ElementOrentationType.vertical},
    {css: two_one, type: ElementType.image, orientation: ElementOrentationType.horizontal},
    {css: two_one, type: ElementType.placeholder, orientation: ElementOrentationType.vertical},
    {css: two_one, type: ElementType.image, orientation: ElementOrentationType.horizontal},
];

export const templateFC5:Array<ElementFactorType> = [
    {css: two_one, type: ElementType.image, orientation: ElementOrentationType.horizontal},
    {css: two_one, type: ElementType.placeholder, orientation: ElementOrentationType.vertical},
    {css: two_one, type: ElementType.image, orientation: ElementOrentationType.horizontal},
    {css: two_one, type: ElementType.image, orientation: ElementOrentationType.horizontal},
    {css: two_one, type: ElementType.image, orientation: ElementOrentationType.horizontal},
    {css: two_one, type: ElementType.image, orientation: ElementOrentationType.horizontal},
];

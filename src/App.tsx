import React from "react";

import './App.css';
import ProductsSection from "./components/ProductsSection";
import {css} from "@emotion/react";
import AppGlobalCss from "./AppGlobalCss";

const App: React.FC<{}> = () => {
    return (
        <div className="App">
            <AppGlobalCss />
            <ProductsSection />
        </div>
  );
}

export default App;

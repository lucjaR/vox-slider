import {SliderPageTemplate, templateFC0, templateFC1, templateFC2, templateFC3, templateFC4, templateFC5} from "../Styles";
import {IImage, SliderPage} from "../Types";

export class SliderModel {
    vImages: IImage[] = [];
    hImages: IImage[] = [];

    constructor(page: SliderPage) {
        page.forEach((image) => {
            if (image.height <= image.width) {
                this.hImages.push(image); //horizontal
            } else {
                this.vImages.push(image); //vertical
            }
        })
    }

    getTemplate = (): SliderPageTemplate => {
        const horizontal = this.hImages.length;
        const vertical = this.vImages.length;
        switch (horizontal) {
            case 0:
                return templateFC0;
            case 1:
                return templateFC1;
            case 2:
                return templateFC2;
            case 3:
                return templateFC3;
            case 4:
                if (vertical >= 4) {
                    return templateFC2;
                } else if (vertical >= 2) {
                    return templateFC3;
                } else {
                    return templateFC4;
                }
            case 5:
            case 6:
            case 7:
            case 8:
                if (vertical >= 4) {
                    return templateFC4;
                } else if (vertical >= 2) {
                    return templateFC3;
                } else {
                    return templateFC5;
                }

            default:
                return templateFC5;
        }
    }
}

import React from "react";
import { Global, css } from "@emotion/react";

const AppGlobalCss = () => (
    <Global
        styles={css`
            html {
                height: 100%;
            }

            body {
                background-color: #a7974c4a;
                min-height: 100vh;
            }

            #root {
                height: 100%;
            }

            html,
            body,
            ul {
                margin: 0;
                padding: 0;
            }
            
            /* Overwrite carousel styles */
            .control-arrow:focus, 
            .carousel.carousel-slider .control-arrow:hover, {
                background: transparent;
            }
        `}
    />
);
export default AppGlobalCss;

import React from "react";
/*@jsxImportSource @emotion/react */
import { css, jsx } from "@emotion/react";

import {IImage} from "../Types";

type PropsType ={
    image: IImage;
};
const Thumbnail: React.FC<PropsType> = ({image}) => {
    return (
        <img src={image.path} css={imgStyles} alt="img"/>
    );
};

export default Thumbnail;

const imgStyles = css`
  height: 80px;
  object-fit: cover;
  object-position: center;
`;

import React, {Fragment, useState} from "react";
import styled from '@emotion/styled/macro';
/*@jsxImportSource @emotion/react */
import {jsx, css} from "@emotion/react";

import {IProduct} from "../Types";

type PropsType = {
    products: IProduct[] | null;
};

const ProductPlacement: React.FC<PropsType> = ({products}) => {
    const [inHover, setHover] = useState("");
    if (!products) {
        return null;
    }
    const placementsToRender = products.map((product: IProduct) => {
        let placementValue = product.placement.split(';');
        let left = placementValue[1];
        let top = placementValue[0];

        return <div onMouseEnter={() => setHover(product.href)}
                    onMouseLeave={() => setHover("")}>
            <PicturePlacement key={product.href} href={`https://www.vox.pl${product.href}`}
                              target="_blank" left={parseFloat(left)} top={parseFloat(top)}/>
            {inHover === product.href &&
                <Popup left={parseFloat(left)} top={parseFloat(top)}>{product.title}</Popup>
            }
        </div>;
    });

    return <Fragment>{placementsToRender}</Fragment>;
}

export default ProductPlacement;

type PlacementPropsType = {
    left: number;
    top: number;
};
const Popup = styled('div')<PlacementPropsType>`
  background-color: white;
  border-radius: 5px;
  position: absolute;
  opacity: 0.8;
  padding: 10px;
  white-space: nowrap;
  top: ${props =>  5 + props.top}%;
  left: ${props => props.left}%;
`;
const PicturePlacement = styled('a')<PlacementPropsType>`
  position: absolute;
  top: ${props => props.top}%;
  left: ${props => props.left}%;
  display: block;
  width: 8px;
  height: 8px;
  border-radius: 50%;
  background-color: seashell;
  opacity: 0.9;
  cursor: pointer;
  animation: pulse 2s infinite;

  @keyframes pulse {
    0% {
      height: 8px;
      width: 8px;
    }
    50% {
      height: 16px;
      width: 16px;
      margin: -4px;
    }
    100% {
      height: 8px;
      width: 8px;
    }
  }
`;

import React from "react";
/*@jsxImportSource @emotion/react */
import { css, jsx } from "@emotion/react";

import {breakpoints} from "../Styles";

type PropsType ={
    className: string;
};

const SliderDescriptionElement: React.FC<PropsType> = ({className}) => {
    return (
        <li className={className}>
            <div css={wrapperStyles}>
                <h3 css={headerStyles}>PALETA ZIEMI</h3>
                <p css={paragraphStyles}>Wypełnij swoją przestrzeń światłem i najcieplejszymi kolorami natury.</p>
            </div>
        </li>
    );
};

export default SliderDescriptionElement;

const wrapperStyles = css`
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: center;
  padding: 5% 0;
  text-align: left;
  width: 100%;
  
  @media (min-width: ${breakpoints.tablet}) {
    padding: 10% 0;
    font-size: 18px;
  }
`;
const headerStyles = css`
  font-size: 14px;

  @media (min-width: ${breakpoints.tablet}) {
    font-size: 20px;
  }  
`;
const paragraphStyles = css`
  font-size: 12px;

  @media (min-width: ${breakpoints.tablet}) {
    font-size: 14px;
  }  
`;

import React, {useEffect, useState} from "react";
/*@jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

import {ISlider, SliderPage} from "./../Types";
import * as voxApi from "./../services/voxApi";
import Thumbnail from "./Thumbnail";
import Slider from "./Slider";

const ProductsSection: React.FC<{}> = () => {
    const [slider, setSlider] = useState<ISlider | undefined>(undefined);
    const [error, setError] = useState(false);

    useEffect(() => {
        voxApi.getSlider()
            .then(setSlider)
            .catch(setError)
    }, []);

    if (!slider) {
        return <div css={loadingStyles}>Loading ...</div>;
    }

    const renderThumbs = (children: React.ReactChild[]): Array<React.ReactChild> =>  {
        if (slider) {
            return slider.pages.map((slider: SliderPage) => <Thumbnail key={slider[0].path} image={slider[0]} />);
        }
        return [];
    }
    return (
        <section css={sectionStyles}>
            {error && <p css={errorStyles}>Error occured</p>}
            {slider &&
                <Carousel renderThumbs={renderThumbs} showStatus={false} infiniteLoop={true} showArrows={true} showIndicators={false}>
                    {slider.pages.map((page: SliderPage) => <Slider images={page} />)}
                </Carousel>
            }
        </section>
    );
}

export default ProductsSection;

const sectionStyles = css`
  height: 100%;
  width: 100%;  
`;
const loadingStyles = css`
  align-items: center;
  display: flex;
  height: 100vh;
  justify-content: center;
`;
const errorStyles = css`
  ${loadingStyles}
  color: red;
`;

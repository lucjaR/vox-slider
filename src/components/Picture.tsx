import React from "react";
/*@jsxImportSource @emotion/react */
import {css, jsx} from "@emotion/react";

import {IImage, IProduct} from "../Types";
import {SelectedImageType} from "./Slider";
import ProductPlacement from "./ProductPlacement";

type PropsType = {
    image: IImage;
    onSelectPicture: (image: SelectedImageType) => void;
    className: string;
};

class Picture extends React.Component<PropsType, {}> {
    pictureRef: React.RefObject<HTMLImageElement>;
    constructor(props: PropsType) {
        super(props);
        this.pictureRef = React.createRef<HTMLImageElement>();
    }

    render() {
        const {image, className, onSelectPicture} = this.props;
        const products: Array<IProduct> = image.products ? image.products : [];

        const handlePictureSelect = () => {
            let pictureNode = this.pictureRef.current;
            if (!pictureNode) {
                return;
            }
            const selectedImage: SelectedImageType = {
                image,
                width: pictureNode.offsetWidth,
                height: pictureNode.offsetHeight
            }
            onSelectPicture(selectedImage);
        }

        return (
            <li onClick={handlePictureSelect} className={className}>
                <ProductPlacement products={products}/>
                <img src={image.path} alt="img" css={imgStyles} ref={this.pictureRef}></img>
            </li>
        );
    }
};

export default Picture;

const imgStyles = css`
  cursor: pointer;
  height: 100%;
  object-fit: cover;
  object-position: center;
  width: 100%;
`;

import React, {useState} from "react";
/** @jsxImportSource @emotion/react */
import {css} from "@emotion/react";
import * as emotionCss from "@emotion/css";
import styled from '@emotion/styled/macro';

import {IImage} from "../Types";
import Picture from "./Picture";
import {breakpoints, ElementOrentationType, ElementType} from "../Styles";
import SliderDescriptionElement from "./SliderDescriptionElement";
import {SliderModel} from "../models/SliderModel"
import ProductPlacement from "./ProductPlacement";

type PropsType = {
    images?: IImage[];
};
export type SelectedImageType = {
    image: IImage;
    width: number;
    height: number;
};
const Slider: React.FC<PropsType> = ({images}) => {
    let [selectedPicture, setSelected] = useState<SelectedImageType | undefined>(undefined);

    if (!images) {
        return null;
    }

    const handleSelect = (img: SelectedImageType) => {
        setSelected(img);
    };
    const handleCloseSelectedPicture = () => {
        setSelected(undefined);
    };

    let sliderModel = new SliderModel(images);
    const pictures = sliderModel.getTemplate().map((element, ind) => {
        let image = element.orientation === ElementOrentationType.horizontal ? sliderModel.hImages.pop() : sliderModel.vImages.pop();
        if (element.type === ElementType.image && image !== undefined) {
            return <Picture key={ind} className={emotionCss.css(element.css)} image={image} onSelectPicture={handleSelect}/>;
        }
        return <SliderDescriptionElement key={ind} className={emotionCss.css(element.css)}/>;
    });

    return (
        <div css={sliderPageStyles}>
            <ul css={listStyles}>
                {pictures}
            </ul>
            {selectedPicture && (
                <div css={selectedSliderPageStyles}>
                    <div css={selectedImgStyles} onClick={handleCloseSelectedPicture}>
                        <ProductPlacement products={selectedPicture.image.products}/>
                        <SelectedImage src={selectedPicture.image.path} alt="img" width={selectedPicture.width} height={selectedPicture.height}/>
                    </div>
                </div>
            )}
        </div>
    );
};

export default Slider;

const sliderPageStyles = css`
  position: relative;
`;
const listStyles = css`
  display: grid;
  list-style: none;
  margin: 0;
  padding: 5% 10%;

    grid-template-columns: repeat(2, 1fr);
    gap: 10px;
    grid-auto-rows: 200px;

  @media (min-width: ${breakpoints.tablet}) {
    grid-template-columns: repeat(2, 1fr);
    gap: 10px;
    grid-auto-rows: 300px;
  }

  @media (min-width: ${breakpoints.desktop}) {
    grid-template-columns: repeat(6, 1fr);
    gap: 10px;
    grid-auto-rows: 300px;
  }
`;

const selectedSliderPageStyles = css`
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  align-items: center;
  top: 0;
  margin: auto;
  width: 100%;
  height: 100%;
`;
const selectedImgStyles = css`
  align-items: center;
  display: flex;
  justify-content: center;
  position: relative;
  cursor: pointer;
`;

type SelectedImagePropsType = {
    height: number;
    width: number;
}
const SelectedImage = styled('img')<SelectedImagePropsType>`
  height: ${props => 2 * props.height}px !important;
  object-fit: cover;
  object-position: center;
  width: ${props => 2 * props.width}px !important;
`;

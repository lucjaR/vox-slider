import {SliderPage, ISlider} from "./../Types";

export const getSliderPage = async (num: number): Promise<SliderPage> => {
    const response = await fetch(`https://www.vox.pl/test/images/${num}`, {method: "GET"})
    if (response.ok) {
        if (response.status === 200) {
        const page: SliderPage = await response.json();
        return page;
        }
        else {
            return Promise.reject(new Error("ups"));
        }
    } else {
        return Promise.reject(new Error("ups"));
    }
}

export const getSlider = async (): Promise<ISlider> => {
    const pagesNumbers = Array.from(Array(7).keys())
    const pages: SliderPage[] = await Promise.all(pagesNumbers.map(getSliderPage));
    return {
        pages
    };
}


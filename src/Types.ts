
export interface IImage {
    path:     string;
    width:    number;
    height:   number;
    products: IProduct[] | null;
}

export interface IProduct {
    href:      string;
    title:     string;
    placement: string;
}

export type SliderPage = IImage[];

export interface ISlider {
    pages: SliderPage[],
}
